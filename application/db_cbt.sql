-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 16, 2020 at 06:35 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_cbt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `hp` varchar(255) DEFAULT NULL,
  `user_group` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `nama`, `alamat`, `hp`, `user_group`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '-', '-', 'Admin'),
(3, 'admin2', '21232f297a57a5a743894a0e4a801fc3', 'admin2', NULL, NULL, 'Admin'),
(4, 'admin2', 'admin', 'admin2', NULL, NULL, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hasil_ujian`
--

CREATE TABLE `tbl_hasil_ujian` (
  `id_hasil` int(11) NOT NULL,
  `id_ujian` int(11) DEFAULT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_hasil_ujian`
--

INSERT INTO `tbl_hasil_ujian` (`id_hasil`, `id_ujian`, `id_peserta`, `nilai`) VALUES
(3, 7, 7, '0'),
(4, 7, 7, '20'),
(5, 7, 7, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mapel`
--

CREATE TABLE `tbl_mapel` (
  `id_mapel` int(11) NOT NULL,
  `mapel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_mapel`
--

INSERT INTO `tbl_mapel` (`id_mapel`, `mapel`) VALUES
(1, 'MTK'),
(2, 'IPA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peserta`
--

CREATE TABLE `tbl_peserta` (
  `id_peserta` int(11) NOT NULL,
  `nisn` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT '-',
  `no_hp` varchar(255) DEFAULT '-',
  `ayah` varchar(255) DEFAULT '-',
  `ibu` varchar(255) DEFAULT '-',
  `wali` varchar(255) DEFAULT '-',
  `id_ujian` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status_ujian` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_peserta`
--

INSERT INTO `tbl_peserta` (`id_peserta`, `nisn`, `nama`, `tgl_lahir`, `alamat`, `no_hp`, `ayah`, `ibu`, `wali`, `id_ujian`, `password`, `status_ujian`) VALUES
(7, '123', 'Zuhri', '1998-12-15', 'Bandar Lampung', '081273735544', 'Rulli', 'Fera', '-', 7, '827ccb0eea8a706c4c34a16891f84e7b', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `jwaban` int(2) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `id_ujian` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soal`
--

INSERT INTO `tbl_soal` (`id`, `soal`, `a`, `b`, `c`, `jwaban`, `gambar`, `id_ujian`) VALUES
(1, 'Apa nama latin dari gambar di bawah ini?', 'Tamarindus indica', 'Averrhoa bilimbi', 'Phyllanthus acidus', 1, 'asam.jpg', NULL),
(2, 'Apa nama latin dari gambar di bawah ini?', 'Tamarindus indica', 'Averrhoa bilimbi', 'Phyllanthus acidus', 2, 'belimbingwuluh.jpg', NULL),
(3, 'Apa nama latin dari gambar di bawah ini?', 'Tamarindus indica', 'Averrhoa bilimbi', 'Phyllanthus acidus', 3, 'cermai.jpg', NULL),
(4, 'Apa nama latin dari gambar di bawah ini?', 'Uncaria gambir', 'Jatropha curcas', 'Citrus x hystrix', 1, 'gambir.jpg', NULL),
(5, 'Apa nama latin dari gambar di bawah ini?', 'Uncaria gambir', 'Jatropha curcas', 'Citrus x hystrix', 2, 'jarak_pagar.jpg', NULL),
(6, 'Apa nama latin dari gambar di bawah ini?', 'Uncaria gambir', 'Jatropha curcas', 'Citrus x hystrix', 3, 'jeruk_purut.jpg', NULL),
(7, 'Apa nama latin dari gambar di bawah ini?', 'Vigna radiata', 'Cocos nucifera', 'Stelechocarpus burahol', 1, 'kacang.jpg', NULL),
(8, 'Apa nama latin dari gambar di bawah ini?', 'Vigna radiata', 'Cocos nucifera', 'Stelechocarpus burahol', 2, 'kelapa.jpg', NULL),
(9, 'Apa nama latin dari gambar di bawah ini?', 'Vigna radiata', 'Cocos nucifera', 'Stelechocarpus burahol', 3, 'kepel.jpg', NULL),
(10, 'Apa nama latin dari gambar di bawah ini?', 'Curcuma longa', 'Alpinia galanga', 'Passiflora edulis', 1, 'kunyit.jpg', NULL),
(11, 'Apa nama latin dari gambar di bawah ini?', 'Curcuma longa', 'Alpinia galanga', 'Passiflora edulis', 2, 'lengkuas.jpg', NULL),
(12, 'Apa nama latin dari gambar di bawah ini?', 'Curcuma longa', 'Alpinia galanga', 'Passiflora edulis', 3, 'markisa.jpg', NULL),
(13, 'Apa nama latin dari gambar di bawah ini?', 'Cucumis sativus', 'Nephelium lappaceum', 'Citrullus lanatus', 1, 'mentimun.jpg', NULL),
(14, 'Apa nama latin dari gambar di bawah ini?', 'Cucumis sativus', 'Nephelium lappaceum', 'Citrullus lanatus', 2, 'rambutan.jpg', NULL),
(15, 'Apa nama latin dari gambar di bawah ini?', 'Cucumis sativus', 'Nephelium lappaceum', 'Citrullus lanatus', 3, 'semangka.jpg', NULL),
(16, 'Apa nama latin dari gambar di bawah ini?', 'Piper betle', 'Curcuma xanthorrhiza', 'Zea mays ssp. mays', 1, 'sirih.jpg', NULL),
(17, 'Apa nama latin dari gambar di bawah ini?', 'Piper betle', 'Curcuma xanthorrhiza', 'Zea mays ssp. mays', 2, 'temulawak.jpg', NULL),
(18, 'Apa nama latin dari gambar di bawah ini?', 'Piper betle', 'Curcuma xanthorrhiza', 'Zea mays ssp. mays', 3, 'jagung.jpg', NULL),
(19, 'Apa nama latin dari gambar di bawah ini?', 'Solanum lycopersicum', 'Orthosiphon aristatus', 'Piper betle', 1, 'tomat.jpg', NULL),
(20, 'Apa nama latin dari gambar di bawah ini?', 'Solanum lycopersicum', 'Orthosiphon aristatus', 'Piper betle', 2, 'kumiskucing.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal2`
--

CREATE TABLE `tbl_soal2` (
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) DEFAULT NULL,
  `id_mapel` int(11) DEFAULT NULL,
  `soal` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `jawaban` varchar(255) DEFAULT NULL,
  `pil1` varchar(255) DEFAULT NULL,
  `pil2` varchar(255) DEFAULT NULL,
  `pil3` varchar(255) DEFAULT NULL,
  `pil4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_soal2`
--

INSERT INTO `tbl_soal2` (`id_soal`, `id_ujian`, `id_mapel`, `soal`, `gambar`, `jawaban`, `pil1`, `pil2`, `pil3`, `pil4`) VALUES
(29, 7, 1, '1 + 1 adalah', NULL, '1', '2', '3', '4', '5'),
(30, 7, 2, 'Anakan Katak disebut', NULL, '2', 'Burung', 'Ayam', 'Kecebong', 'Kucing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ujian`
--

CREATE TABLE `tbl_ujian` (
  `id_ujian` int(11) NOT NULL,
  `nama_ujian` varchar(255) DEFAULT NULL,
  `tgl_ujian` date DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `stat` tinyint(1) DEFAULT '0',
  `waktu` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_ujian`
--

INSERT INTO `tbl_ujian` (`id_ujian`, `nama_ujian`, `tgl_ujian`, `ket`, `stat`, `waktu`) VALUES
(7, 'Ujian Masuk SMP', '2020-12-15', 'Tahun 2020', 0, '30');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_peserta_ujian`
-- (See below for the actual view)
--
CREATE TABLE `vw_peserta_ujian` (
`nisn` varchar(255)
,`nama` varchar(255)
,`tgl_lahir` date
,`alamat` varchar(255)
,`no_hp` varchar(255)
,`ayah` varchar(255)
,`ibu` varchar(255)
,`wali` varchar(255)
,`id_ujian` int(11)
,`password` varchar(255)
,`nama_ujian` varchar(255)
,`tgl_ujian` date
,`stat` tinyint(1)
,`ket` varchar(255)
,`waktu` varchar(255)
,`status_ujian` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_soal`
-- (See below for the actual view)
--
CREATE TABLE `vw_soal` (
`id_soal` int(11)
,`id_ujian` int(11)
,`nama_ujian` varchar(255)
,`tgl_ujian` date
,`waktu` varchar(255)
,`id_mapel` int(11)
,`mapel` varchar(255)
,`soal` varchar(255)
,`gambar` varchar(255)
,`jawaban` varchar(255)
,`pil1` varchar(255)
,`pil2` varchar(255)
,`pil3` varchar(255)
,`pil4` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_soal_2`
-- (See below for the actual view)
--
CREATE TABLE `vw_soal_2` (
`nama_ujian` varchar(255)
,`tgl_ujian` date
,`waktu` varchar(255)
,`id` int(5)
,`soal` text
,`a` text
,`b` text
,`c` text
,`jwaban` int(2)
,`gambar` varchar(50)
,`id_ujian` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_peserta_ujian`
--
DROP TABLE IF EXISTS `vw_peserta_ujian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_peserta_ujian`  AS  select `tbl_peserta`.`nisn` AS `nisn`,`tbl_peserta`.`nama` AS `nama`,`tbl_peserta`.`tgl_lahir` AS `tgl_lahir`,`tbl_peserta`.`alamat` AS `alamat`,`tbl_peserta`.`no_hp` AS `no_hp`,`tbl_peserta`.`ayah` AS `ayah`,`tbl_peserta`.`ibu` AS `ibu`,`tbl_peserta`.`wali` AS `wali`,`tbl_peserta`.`id_ujian` AS `id_ujian`,`tbl_peserta`.`password` AS `password`,`tbl_ujian`.`nama_ujian` AS `nama_ujian`,`tbl_ujian`.`tgl_ujian` AS `tgl_ujian`,`tbl_ujian`.`stat` AS `stat`,`tbl_ujian`.`ket` AS `ket`,`tbl_ujian`.`waktu` AS `waktu`,`tbl_peserta`.`status_ujian` AS `status_ujian` from (`tbl_peserta` join `tbl_ujian` on((`tbl_peserta`.`id_ujian` = `tbl_ujian`.`id_ujian`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_soal`
--
DROP TABLE IF EXISTS `vw_soal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_soal`  AS  select `tbl_soal2`.`id_soal` AS `id_soal`,`tbl_ujian`.`id_ujian` AS `id_ujian`,`tbl_ujian`.`nama_ujian` AS `nama_ujian`,`tbl_ujian`.`tgl_ujian` AS `tgl_ujian`,`tbl_ujian`.`waktu` AS `waktu`,`tbl_mapel`.`id_mapel` AS `id_mapel`,`tbl_mapel`.`mapel` AS `mapel`,`tbl_soal2`.`soal` AS `soal`,`tbl_soal2`.`gambar` AS `gambar`,`tbl_soal2`.`jawaban` AS `jawaban`,`tbl_soal2`.`pil1` AS `pil1`,`tbl_soal2`.`pil2` AS `pil2`,`tbl_soal2`.`pil3` AS `pil3`,`tbl_soal2`.`pil4` AS `pil4` from ((`tbl_soal2` join `tbl_ujian` on((`tbl_soal2`.`id_ujian` = `tbl_ujian`.`id_ujian`))) join `tbl_mapel` on((`tbl_soal2`.`id_mapel` = `tbl_mapel`.`id_mapel`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_soal_2`
--
DROP TABLE IF EXISTS `vw_soal_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_soal_2`  AS  select `tbl_ujian`.`nama_ujian` AS `nama_ujian`,`tbl_ujian`.`tgl_ujian` AS `tgl_ujian`,`tbl_ujian`.`waktu` AS `waktu`,`tbl_soal`.`id` AS `id`,`tbl_soal`.`soal` AS `soal`,`tbl_soal`.`a` AS `a`,`tbl_soal`.`b` AS `b`,`tbl_soal`.`c` AS `c`,`tbl_soal`.`jwaban` AS `jwaban`,`tbl_soal`.`gambar` AS `gambar`,`tbl_soal`.`id_ujian` AS `id_ujian` from (`tbl_soal` join `tbl_ujian` on((`tbl_soal`.`id_ujian` = `tbl_ujian`.`id_ujian`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hasil_ujian`
--
ALTER TABLE `tbl_hasil_ujian`
  ADD PRIMARY KEY (`id_hasil`) USING BTREE,
  ADD KEY `id_ujian` (`id_ujian`),
  ADD KEY `id_peserta` (`id_peserta`);

--
-- Indexes for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tbl_peserta`
--
ALTER TABLE `tbl_peserta`
  ADD PRIMARY KEY (`id_peserta`) USING BTREE,
  ADD KEY `id_ujian` (`id_ujian`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ujian` (`id_ujian`);

--
-- Indexes for table `tbl_soal2`
--
ALTER TABLE `tbl_soal2`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `id_ujian` (`id_ujian`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `tbl_ujian`
--
ALTER TABLE `tbl_ujian`
  ADD PRIMARY KEY (`id_ujian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_hasil_ujian`
--
ALTER TABLE `tbl_hasil_ujian`
  MODIFY `id_hasil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_peserta`
--
ALTER TABLE `tbl_peserta`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_soal2`
--
ALTER TABLE `tbl_soal2`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_ujian`
--
ALTER TABLE `tbl_ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_hasil_ujian`
--
ALTER TABLE `tbl_hasil_ujian`
  ADD CONSTRAINT `tbl_hasil_ujian_ibfk_1` FOREIGN KEY (`id_ujian`) REFERENCES `tbl_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_hasil_ujian_ibfk_2` FOREIGN KEY (`id_peserta`) REFERENCES `tbl_peserta` (`id_peserta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_peserta`
--
ALTER TABLE `tbl_peserta`
  ADD CONSTRAINT `id_ujian` FOREIGN KEY (`id_ujian`) REFERENCES `tbl_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_soal2`
--
ALTER TABLE `tbl_soal2`
  ADD CONSTRAINT `tbl_soal2_ibfk_1` FOREIGN KEY (`id_ujian`) REFERENCES `tbl_ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_soal2_ibfk_2` FOREIGN KEY (`id_mapel`) REFERENCES `tbl_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;
