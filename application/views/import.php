<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <style type="text/css">
	body{
		font-family: sans-serif;
        margin: 20px;
	}
 
	p{
		color: green;
	}
</style>
<?php if ($this->session->flashdata('message')): ?> 
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php endif; ?>
    <h2>IMPORT USERS</h2>
    
        <form method="post" enctype="multipart/form-data" action="<?php echo site_url('import/import_excel') ?>">
            <div class="input-group mb-3">
                <div class="row">
                    <div class="col">
                        <input name="fileExcel" id="fileExcel" type="file" required="required" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"> 
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="row">
                    <div class="col">
                        <input class="btn btn-success" name="upload" type="submit" value="Import">
                    </div>
                </div>
            </div>
        </form>

    <div class="input-group mb-3">
        <iframe id="my_iframe" style="display:none;"></iframe>
        <a class="btn btn-outline-primary" onclick="download_template()" href="#">Download Template</a>
    </div>

    <script>
        function download_template() {
            console.log('<?php echo site_url('assets/template_user_cbt.xlsx');?>');
            document.getElementById('my_iframe').src = '<?php echo site_url('assets/template_user_cbt.xlsx');?>';
        }
        
    </script>
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
     <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</body>
    <script type="text/javascript">

       window.setTimeout(function() {
            $(".alert").fadeTo(10, 0).slideUp(50, function(){
                $(this).remove(); 
            });
        }, 5000);

    </script>
</html>