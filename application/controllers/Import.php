<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Peserta_model');
    $this->load->library(array('excel','session'));
  }

  public function index(){
    $this->load->view("import");
  }

  public function import_excel(){
        if (isset($_FILES["fileExcel"]["name"])) {
            $path = $_FILES["fileExcel"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            $data = array();
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    
                    if($worksheet->getCellByColumnAndRow(0, $row)->getValue() != ""){
                        $obj=new stdClass;
                        $obj->nisn=$worksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $obj->nama=$worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $obj->tgl_lahir=date("Y-m-d",strtotime($worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue()));
                        $obj->alamat=$worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $obj->no_hp=$worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $obj->ayah=$worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $obj->ibu=$worksheet->getCellByColumnAndRow(6, $row)->getValue();
                        $obj->wali=$worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        $obj->id_ujian=$worksheet->getCellByColumnAndRow(8, $row)->getValue();
                        $obj->password=$worksheet->getCellByColumnAndRow(9, $row)->getValue();
                        array_push($data,$obj);
                    }
                    
                }
            }
            $this->db->trans_begin();
            $insert = $this->Peserta_model->insert_batch($data);
            if($insert > 0){
                $this->db->trans_commit();
                $this->session->set_flashdata('message', 'Data berhasil diimport');
            }else{
                $this->db->trans_rollback();
                $this->session->set_flashdata('message', 'Data gagal diimport');
            }
            redirect('import');
        }else{
            $this->session->set_flashdata('fail', 'Tidak ada file yang di import');
        }
	}
}