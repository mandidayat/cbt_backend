<?php

require APPPATH . 'libraries/REST_Controller.php';

class Ujian extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model ('Ujian_model');
    }

    public function index_get(){
        $this->response(array("message" => "ok","data"=>$this->Ujian_model->get_all()->result()));   
    }

    public function listUjian_post(){
        if(!empty($this->post("nisn"))){
            $this->response(array("message" => "ok","data"=>$this->Ujian_model->listUjian($this->post("nisn"))->result()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function add_post(){
        if(!empty($this->post("nama")) && !empty($this->post("tgl")) && !empty($this->post("waktu"))){
            $this->response($this->Ujian_model->add($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function update_post(){
        if(!empty($this->post("nama")) && !empty($this->post("tgl")) && !empty($this->post("ket")) && !empty($this->post("waktu")) && !empty($this->post("id"))){
            $this->response($this->Ujian_model->update($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function delete_post(){
        if(!empty($this->post("id"))){
            $this->response($this->Ujian_model->delete($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function simpanNilai_post(){
        if(!empty($this->post("id_ujian")) && !empty($this->post("id_peserta"))){
            
            $this->response($this->Ujian_model->addHasil($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

}

?>
