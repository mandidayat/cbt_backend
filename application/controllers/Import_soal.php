<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import_soal extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Soal_model');
    $this->load->library(array('excel','session'));
  }

  public function index(){
    $this->load->view("import_soal");
  }

  public function import_excel(){
        if (isset($_FILES["fileExcel"]["name"])) {
            $path = $_FILES["fileExcel"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            $data = array();
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    
                    if($worksheet->getCellByColumnAndRow(0, $row)->getValue() != ""){
                        $obj=new stdClass;
                        $obj->id_ujian=$worksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $obj->id_mapel=$worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $obj->soal=$worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $obj->jawaban=$worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $obj->pil1=$worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $obj->pil2=$worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $obj->pil3=$worksheet->getCellByColumnAndRow(6, $row)->getValue();
                        $obj->pil4=$worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        array_push($data,$obj);
                    }
                    
                }
            }
            $this->db->trans_begin();
            $insert = $this->Soal_model->insert_batch($data);
            if($insert > 0){
                $this->db->trans_commit();
                $this->session->set_flashdata('message', 'Data berhasil diimport');
            }else{
                $this->db->trans_rollback();
                $this->session->set_flashdata('message', 'Data gagal diimport');
            }
            redirect('import_soal');
        }else{
            $this->session->set_flashdata('fail', 'Tidak ada file yang di import');
        }
	}
}