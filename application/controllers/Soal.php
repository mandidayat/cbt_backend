<?php

require APPPATH . 'libraries/REST_Controller.php';

class Soal extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model ('Soal_model');
    }

    public function index_get(){
        $this->response(array("message" => "ok","data"=>$this->Soal_model->get_all()->result()));   
        // $this->response(array("message" => "ok","data"=>$this->Soal_model->get_all()->result()));   
    }

    public function editFilter_get(){
        if(!empty($this->get("id"))){
            $this->response(array("message" => "ok","data"=>$this->Soal_model->get_filter($this->get("id"))->result()));   
        }else{
            $this->response(array("message" => "ok","data"=>$this->Soal_model->get_all()->result()));   
        }
    }

    public function add_post(){
        if(!empty($this->post("id_ujian")) && !empty($this->post("id_mapel")) && !empty($this->post("soal")) && !empty($this->post("jawaban")) && !empty($this->post("pil1")) && !empty($this->post("pil2")) && !empty($this->post("pil3")) && !empty($this->post("pil4"))){
            $this->response($this->Soal_model->add($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function update_post(){
        if(!empty($this->post("id_soal")) && !empty($this->post("id_ujian")) && !empty($this->post("id_mapel")) && !empty($this->post("soal")) && !empty($this->post("jawaban")) && !empty($this->post("pil1")) && !empty($this->post("pil2")) && !empty($this->post("pil3")) && !empty($this->post("pil4"))){
            $this->response($this->Soal_model->update($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function delete_post(){
        if(!empty($this->post("id"))){
            $this->response($this->Soal_model->delete($this->post("id")));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }
}

?>
