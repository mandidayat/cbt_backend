<?php

require APPPATH . 'libraries/REST_Controller.php';

class Login extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model ('User_model');
    }

    function user_post()
    {
        if(!empty($this->post("username")) && !empty($this->post("password"))){
        $cek = $this->User_model->login_user($this->post());
        if( $cek->num_rows() > 0){
            $data = array('status_login' => "berhasil" , 'message' => 'Login Berhasil');
            $this->response(array('responce'=>TRUE,'login'=>array($data),'data'=>($this->User_model->get_user_detail($this->post()))));
        }else{
            $data = array('status_login' => "gagal" , 'message' => 'Login Anda gagal mohon di periksa kembali' );
            $this->response(array('responce'=>FALSE,'login'=>array($data),'data'=>array(null)));
        }
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    function peserta_post()
    {
        if(!empty($this->post("nisn")) && !empty($this->post("password"))){
        $cek = $this->User_model->login_peserta($this->post());
        if( $cek->num_rows() > 0){
            $data = array('status_login' => "berhasil" , 'message' => 'Login Berhasil');
            $this->response(array('responce'=>TRUE,'login'=>array($data),'data'=>($this->User_model->get_peserta_detail($this->post()))));
        }else{
            $data = array('status_login' => "gagal" , 'message' => 'Login Anda gagal mohon di periksa kembali' );
            $this->response(array('responce'=>FALSE,'login'=>array($data),'data'=>array(null)));
        }
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

}

?>
