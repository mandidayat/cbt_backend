<?php

require APPPATH . 'libraries/REST_Controller.php';

class MP extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model ('MP_model');
    }

    public function index_get(){
        $this->response(array("message" => "ok","data"=>$this->MP_model->get_all()->result()));   
    }

    public function add_post(){
        if(!empty($this->post("nama"))){
            $this->response($this->MP_model->add($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function update_post(){
        if(!empty($this->post("nama")) && !empty($this->post("id"))){
            $this->response($this->MP_model->update($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function delete_post(){
        if(!empty($this->post("id"))){
            $this->response($this->MP_model->delete($this->post("id")));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

}

?>
