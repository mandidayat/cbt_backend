<?php

require APPPATH . 'libraries/REST_Controller.php';

class Peserta extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model ('Peserta_model');
    }

    public function index_get(){
        if(!empty($this->get("id_ujian"))){
            $this->response(array("message" => "ok","data"=>$this->Peserta_model->get_where($this->get("id_ujian"))->result()));   
        }else{
            $this->response(array("message" => "ok","data"=>$this->Peserta_model->get_all()->result()));   
        }
        
    }

    public function add_post(){
        if(!empty($this->post("nisn")) && !empty($this->post("nama")) && !empty($this->post("tgl_lahir")) && !empty($this->post("password")) && !empty($this->post("id_ujian")) ){
            $cek_nisn = $this->Peserta_model->cek_nisn($this->post("nisn"),$this->post("id_ujian"))->num_rows();
            if($cek_nisn){
                $this->response(array("status"=>false,"message" => "NISN Sudah Terdaftar","data" => array()));
            }else{
                $this->response($this->Peserta_model->add($this->post()));
            }
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function update_post(){
        if(!empty($this->post("nisn")) && !empty($this->post("nama")) && !empty($this->post("tgl_lahir")) && !empty($this->post("id_ujian")) && !empty($this->post("id")) ){
            $this->response($this->Peserta_model->update($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

    public function delete_post(){
        if(!empty($this->post("id"))){
            $this->response($this->Peserta_model->delete($this->post()));
        }else{
            $this->response(array("message" => "Data Tidak Lengkap") , 400);
        }
    }

}

?>
