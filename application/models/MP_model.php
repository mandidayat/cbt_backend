<?php
if(! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class MP_model extends CI_Model {
  
  private $_table = "tbl_mapel";
  private $_message_gagal = "Data Gagal Di input";
  private $_message_succes = "Data Berhasil Di input";

  public function get_all(){					
    $sqlx=$this->db->query("SELECT * FROM ".$this->_table);
    return $sqlx;
  }

  public function add($data){
    $this->mapel = $data['nama'];
    if($this->db->insert($this->_table, $this)){
      return array("status"=>true,"message" => $this->_message_succes,"data" => array());
    }else{
      $data = $this->db->error();
      return array("status"=>false,"message" => $this->_message_gagal,"data" => array());
    }
  }

  public function delete($data){
    $this->db->where('id_mapel', $data);
    if($this->db->delete($this->_table)){
        return array("message" => "Data Berhasil Di Hapus","data" => array());
    }else{
        return array("message" => "Data Gagal Di Hapus","data" => array());
    }
  }

  function update($data){
    $data1 = array(
      "mapel" => $data['nama']
    );

    $this->db->where('id_mapel', $data['id']);

    if($this->db->update($this->_table, $data1)){
        return array("message" => "Data Berhasil Di Update","data" => array());
    }else{
        $data = $this->db->error();
        return array("message" => "Data Gagal Di Update","data" => array());
    }

  }

}