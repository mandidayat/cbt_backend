<?php

class Soal_model extends CI_Model {
  
  private $_table = "tbl_soal2";
  private $_table_vw = "vw_soal";
  private $_message_gagal = "Data Gagal Di input";
  private $_message_succes = "Data Berhasil Di input";

  public function get_all(){					
    $sqlx=$this->db->query("SELECT * FROM ".$this->_table_vw);
    return $sqlx;
  }

  public function get_filter($id){
    return $this->db->get_where($this->_table,["id_soal"=>$id]);
  }

  public function get_where($where){
    return $this->db->get_where($this->_table,$where);
  }

  public function add($data){
    $this->id_ujian = $data['id_ujian'];
    $this->id_mapel  = $data['id_mapel'];
    $this->soal  = $data['soal'];
    $this->jawaban  = $data['jawaban'];
    $this->pil1  = $data['pil1'];
    $this->pil2  = $data['pil2'];
    $this->pil3  = $data['pil3'];
    $this->pil4  = $data['pil4'];

    // if(!empty($data["gambar"])){ 
      $this->gambar  = $this->_uploadImage();
    // }

    if($this->db->insert($this->_table, $this)){
      return array("status"=>true,"message" => $this->_message_succes,"data" => array());
    }else{
      $data = $this->db->error();
      return array("status"=>false,"message" => $this->_message_gagal,"data" => array());
    }
  }

  public function delete($data){
    $this->db->where('id_soal', $data);
    if($this->db->delete($this->_table)){
        return array("message" => "Data Berhasil Di Hapus","data" => array());
    }else{
        return array("message" => "Data Gagal Di Hapus","data" => array());
    }
  }

  public function insert_batch($data){
    return $this->db->insert_batch($this->_table, $data);
  }


  function update($data){
    if (!empty($_FILES["gambar"]["name"])) {
      $data1 = array(
        "id_ujian" => $data['id_ujian'],
        "id_mapel" => $data['id_mapel'],
        "soal" => $data['soal'],
        "jawaban" => $data['jawaban'],
        "pil1" => $data['pil1'],
        "pil2" => $data['pil2'],
        "pil3" => $data['pil3'],
        "pil4" => $data['pil4'],
        "gambar" => $this->_uploadImage()
      );
    }else{
      $data1 = array(
        "id_ujian" => $data['id_ujian'],
        "id_mapel" => $data['id_mapel'],
        "soal" => $data['soal'],
        "jawaban" => $data['jawaban'],
        "pil1" => $data['pil1'],
        "pil2" => $data['pil2'],
        "pil3" => $data['pil3'],
        "pil4" => $data['pil4']
      );
    }

    $this->db->where('id_soal', $data['id_soal']);

    if($this->db->update($this->_table, $data1)){
        return array("message" => "Data Berhasil Di Update","data" => array());
    }else{
        $data = $this->db->error();
        return array("message" => "Data Gagal Di Update","data" => array());
    }

  }

  private function _uploadImage()
  {

    $config['upload_path']          = './uploads/soal/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = uniqid();
    $config['overwrite']			      = true;
    $config['max_size']             = 5024; // 1MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('gambar')) {
      return $this->upload->data("file_name");
    }
      
      // return "default.jpg";
  }
}