<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Peserta_model extends CI_Model {

  private $_table = "tbl_peserta";
  private $_vw_ujian_peserta = "vw_peserta_ujian";
  private $_table_hasil = "tbl_hasil_ujian";
  private $_message_gagal = "Data Gagal Di input";
  private $_message_succes = "Data Berhasil Di input";

	function __construct() {

		parent::__construct ();

  }
    
  public function get_all(){
    return $this->db->get($this->_table);
  }

  public function get_where($id_ujian){
    return $this->db->get_where($this->_table,["id_ujian"=>$id_ujian]);
  }

  public function cek_nisn($nisn,$id_ujian){
    return $this->db->get_where($this->_table,["nisn"=>$nisn,"id_ujian"=>$id_ujian]);
  }

  public function getUjianPeserta($id){
    return $this->db->get_where($this->_vw_ujian_peserta,["nisn"=>$id]);
  }

  public function insert_batch($data){
    return $this->db->insert_batch($this->_table, $data);
  }

  public function add($data){
    
    $this->nisn = $data['nisn'];
    $this->tgl_lahir = date('Y-m-d',strtotime($data['tgl_lahir']));
    $this->nama = $data['nama'];
    $this->password = md5($data['password']);
    $this->id_ujian = $data['id_ujian'];
    if(!empty($data["alamat"])){ $this->alamat = $data['alamat'];}
    if(!empty($data["no_hp"])){ $this->no_hp = $data['no_hp'];}
    if(!empty($data["ayah"])){ $this->ayah = $data['ayah'];}
    if(!empty($data["ibu"])){ $this->ibu = $data['ibu'];}
    if(!empty($data["wali"])){ $this->wali = $data['wali'];}

    if($this->db->insert($this->_table, $this)){
      return array("status"=>true,"message" => $this->_message_succes,"data" => array());
    }else{
      return array("status"=>false,"message" => $this->_message_gagal,"data" => array());
    }
  }

  public function update($data){

    if(!empty($data["alamat"])){ $alamat = $data['alamat'];}else{$alamat = null;}
    if(!empty($data["no_hp"])){ $no_hp = $data['no_hp'];}else{$no_hp = null;}
    if(!empty($data["ayah"])){ $ayah = $data['ayah'];}else{$ayah = null;}
    if(!empty($data["ibu"])){ $ibu = $data['ibu'];}else{$ibu = null;}
    if(!empty($data["wali"])){ $wali = $data['wali'];}else{$wali = null;}
    if(!empty($data["password"])){ 
      $dat = array(
        "nisn" => $data["nisn"],
        "tgl_lahir" => date('Y-m-d',strtotime($data["tgl_lahir"])),
        "nama" => $data["nama"],
        "alamat" => $alamat,
        "no_hp" => $no_hp,
        "ayah" => $ayah,
        "ibu" => $ibu,
        "wali" => $wali,
        "id_ujian" => $data["id_ujian"],
        "password" => md5($data["password"])
      );
    }else{
      $dat = array(
        "nisn" => $data["nisn"],
        "tgl_lahir" => date('Y-m-d',strtotime($data["tgl_lahir"])),
        "nama" => $data["nama"],
        "alamat" => $alamat,
        "no_hp" => $no_hp,
        "ayah" => $ayah,
        "ibu" => $ibu,
        "wali" => $wali,
        "id_ujian" => $data["id_ujian"]
      );
    }
      
    $this->db->where('id_peserta', $data['id']);
   
    if( $this->db->update($this->_table, $dat)){
      return array("message" => "Data Berhasil Di Update","data" => array());
    }else{
      return array("message" => "Data Gagal Di Update","data" => array());
    }

  }

  public function delete($data){
    $this->db->where('id_peserta', $data['id']);
    if($this->db->delete($this->_table)){
      return array("message" => "Data Berhasil Di Hapus","data" => array());
    }else{
      $data = $this->db->error();
      return array("message" => "Data Gagal Di Hapus","data" => array());
    }
  }

}