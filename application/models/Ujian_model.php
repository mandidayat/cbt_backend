<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Ujian_model extends CI_Model {

  private $_table = "tbl_ujian";
  private $_vw_ujian_peserta = "vw_peserta_ujian";
  private $_table_hasil = "tbl_hasil_ujian";
private $_message_gagal = "Data Gagal Di Simpan";
  private $_message_succes = "Data Berhasil Disimpan";
	function __construct() {

		parent::__construct ();

  }
    
  public function get_all(){
    return $this->db->get($this->_table);
  }

  public function getUjianPeserta($id){
    return $this->db->get_where($this->_vw_ujian_peserta,["nisn"=>$id]);
  }

  public function listUjian($id){
    return $this->db->get_where($this->_vw_ujian_peserta,["nisn"=>$id]);
  }

  public function addHasil($data){
    
    $this->id_ujian = $data['id_ujian'];
    $this->id_peserta = $data['id_peserta'];
    $this->nilai = $data['nilai'];

    if($this->db->insert($this->_table_hasil, $this)){
      $this->updateUjian($data);
      return array("status"=>true,"message" => $this->_message_succes,"data" => array());
    }else{
     return array("status"=>false,"message" => $this->_message_gagal,"data" => array());
    }
  }

  public function updateUjian($data){
    $dat = array(
      "status_ujian" => "1"
    );
    $this->db->where('id_peserta', $data['id_peserta']);
    $this->db->update("tbl_peserta", $dat);
  
  }

  public function add($data){
    
    $this->nama_ujian = $data['nama'];
    $this->tgl_ujian = date('Y-m-d',strtotime($data['tgl']));
    $this->ket = $data['ket'];
    $this->waktu = $data['waktu'];

    if($this->db->insert($this->_table, $this)){
      $dat = array( "status"=>true);
      return $dat;
    }else{
      $dat = array( "status"=>false);
      $data = $this->db->error();
      return $dat;
    }
  }

  public function update($data){

    $dat = array(
      "nama_ujian" => $data["nama"],
      "tgl_ujian" => date('Y-m-d',strtotime($data["tgl"])),
      "waktu" => $data["waktu"],
      "ket" => $data["ket"]
    );
      
    $this->db->where('id_ujian', $data['id']);
   
    if( $this->db->update($this->_table, $dat)){
      return array("message" => "Data Berhasil Di Update","data" => array());
    }else{
      return array("message" => "Data Gagal Di Update","data" => array());
    }

  }

  public function delete($data){
    $this->db->where('id_ujian', $data['id']);
    if($this->db->delete($this->_table)){
      $this->delete_peserta($data);
      $this->delete_soal($data);
      return array("message" => "Data Berhasil Di Hapus","data" => array());
    }else{
      $data = $this->db->error();
      return array("message" => "Data Gagal Di Hapus","data" => array());
    }
  }

  public function delete_peserta($data){
    $this->db->where('id_ujian', $data['id']);
    $this->db->delete("tbl_peserta");
  }

  public function delete_soal($data){
    $this->db->where('id_ujian', $data['id']);
    $this->db->delete('tbl_soal2');
  }

}