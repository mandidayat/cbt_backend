<?php
if(! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class User_model extends CI_Model {
  
  private $_table_admin = "tbl_admin";
  private $_table_peserta = "tbl_peserta";
  private $_message_gagal = "Data Gagal Di input";
  private $_message_succes = "Data Berhasil Di input";

  public function get_all(){					
    $sqlx=$this->db->query("SELECT * FROM ".$this->_table_admin);
    return $sqlx;
  }

  public function add($data){
    $this->username = $data['username'];
    $this->password  = md5($data['password']);
    if(!empty($data["nama"])){ $this->nama = $data['nama'];}
    if(!empty($data["alamat"])){ $this->alamat = $data['alamat'];}
    if(!empty($data["hp"])){ $this->hp = $data['hp'];}
    $this->user_group = 'Admin';

    if($this->db->insert($this->_table_admin, $this)){
      return array("status"=>true,"message" => $this->_message_succes,"data" => array());
    }else{
      $data = $this->db->error();
      return array("status"=>false,"message" => $this->_message_gagal,"data" => array());
    }
  }

  public function delete($data){
    $this->db->where('id', $data);
    if($this->db->delete($this->_table_admin)){
        return array("message" => "Data Berhasil Di Hapus","data" => array());
    }else{
        return array("message" => "Data Gagal Di Hapus","data" => array());
    }
  }

  function update($data){
    if(!empty($data["password"])){ 
      $data1 = array(
        "username" => $data['username'],
        "password" => md5($data['password']),
        "nama" => $data['nama']
      );
    }else{
      $data1 = array(
        "username" => $data['username'],
        "nama" => $data['nama']
      );
    }

    $this->db->where('id', $data['id']);

    if($this->db->update($this->_table_admin, $data1)){
        return array("message" => "Data Berhasil Di Update","data" => array());
    }else{
        $data = $this->db->error();
        return array("message" => "Data Gagal Di Update","data" => array());
    }

  }

  public function registrasi_user($data){		
    $pw = random_string('alpha', 8);
    $id_user = uniqid();
    $this->id_user = $id_user;
    $this->username = $data['email'];
    $this->password = md5($pw);
    $this->nama_lengkap = $data['nama'];
    $this->no_telp = $data['no_hp'];
    $this->email = $data['email'];
    $this->tanggal_daftar = date('Y-m-d H:i:s');

    if($this->db->insert('tbl_users', $this)){
      $dat = array( "status"=>true,"id_user" => $id_user,"id_refrensi" => $data['id_refrensi'],"pw" => $pw, "email" => $data['email']);
      return $dat;
    }else{
      $dat = array( "status"=>false,"id_user" => $id_user,"pw",$pw);
      $data = $this->db->error();
      return $dat;
    }
  }

  public function login_user($data){			
    
    $this->db->select('*');
    $this->db->from($this->_table_admin);
    $this->db->where('username',$data['username']);
    $this->db->where('password=',md5($data['password']));
    
    return $query = $this->db->get();
  }

  public function login_peserta($data){			
    
    $this->db->select('*');
    $this->db->from($this->_table_peserta);
    $this->db->where('nisn',$data['nisn']);
    $this->db->where('password=',md5($data['password']));
    
    return $this->db->get();
  }

  public function get_user_detail($data){			
      
    $this->db->select('*');
    $this->db->from($this->_table_admin);
    $this->db->where('username',$data['username']);
    
    return $query = $this->db->get()->result();
  }

   public function get_peserta_detail($data){			
      
    $this->db->select('*');
    $this->db->from($this->_table_peserta);
    $this->db->where('nisn',$data['nisn']);
    
    return $query = $this->db->get()->result();
  }


}